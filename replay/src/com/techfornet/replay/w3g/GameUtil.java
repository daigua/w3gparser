package com.techfornet.replay.w3g;

import java.sql.Time;
import java.text.DateFormat;
import java.util.List;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
//import java.util.zip.InflaterOutputStream;


import com.jcraft.jzlib.JZlib;
import com.jcraft.jzlib.ZStream;

public class GameUtil implements Serializable{
	public static int toNoSymbolInt(int i){
		return i<0?i+256:i;
	}
	public static byte[] uncompressData(byte[] data,int len) {
		int err;
		int uncomprLen = len*100;
		byte[] uncompr = new byte[uncomprLen];
		ZStream d_stream = new ZStream();
		err = d_stream.inflateInit();
		d_stream.next_in = data;
		d_stream.next_in_index = 0;
		d_stream.next_out = uncompr;
		d_stream.next_out_index = 0;
		
		while (true) {
			 if(d_stream.total_out==uncompr.length){
				 int tempLen=uncompr.length;
				 byte[] tempAry=new byte[tempLen];
				 System.arraycopy(uncompr, 0, tempAry, 0, tempLen);
				 uncompr=null;
				 uncompr=new byte[tempLen+uncomprLen];
				 System.arraycopy(tempAry, 0, uncompr, 0, tempLen);
				 d_stream.next_out = uncompr;
			 }
			 d_stream.avail_in = d_stream.avail_out = 1;
			 err = d_stream.inflate(JZlib.Z_SYNC_FLUSH);
			 //System.out.println(err);
			 if(err==JZlib.Z_DATA_ERROR){
				break;
			 }
			 if (err == JZlib.Z_STREAM_END) {
				 break;
			 }
		}
		err = d_stream.inflateEnd();
		byte[] unzipfile = new byte[(int) d_stream.total_out];
		System.arraycopy(uncompr, 0, unzipfile, 0, unzipfile.length);
		return unzipfile;
	}
	/*
	public static String decompressData(String data){
		try{
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			InflaterOutputStream zos=new InflaterOutputStream(bos);
			zos.write(GameUtil.convertFromBase64(data));
			zos.close();
			return new String(bos.toByteArray());
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static byte[] convertFromBase64(String data){
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] b=null;
		try {
			b = decoder.decodeBuffer(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			//e.printStackTrace();
		}
		//System.out.println(new String(b));
		return b;
	}
	public static String getBASE64(String s) { 
		if (s == null) return null; 
		return (new sun.misc.BASE64Encoder()).encode( s.getBytes() ); 
	} 
	public static byte[] getFromBASE64(String s) { 
		if (s == null) return null; 
			BASE64Decoder decoder = new BASE64Decoder(); 
		try { 
			byte[] b = decoder.decodeBuffer(s); 
			return b;
		} catch (Exception e) { 
			return null; 
		} 
	}*/
	public static String byteToString(byte[] b,int sta,int len){
		String temp="";
		//System.out.println(len);
		byte[] tempb=new byte[len];
		System.arraycopy(b, sta, tempb, 0, len);
		return new String(tempb);
	}
	public static byte[] subAry(byte[] b,int sta,int len){
		 byte[] temp=new byte[len];
		 //System.out.println(b.length-sta-len);
		 System.arraycopy(b, sta, temp, 0, len);
		 return temp;
	}
	public static byte[]  getW3gData(String src,int bSize) throws IOException{
		ArrayList<Byte> al=new ArrayList();
		FileChannel in=new FileInputStream(src).getChannel();
		ByteBuffer buffer=ByteBuffer.allocate(bSize);
		while(in.read(buffer) !=-1){
			buffer.flip();
			while(buffer.hasRemaining()){
				al.add((Byte)buffer.get());
			}
			buffer.clear();
		}
		in.close();
		int length=al.size();
		byte[] temp=new byte[length];
		for(int i=0;i<length;i++){
			temp[i]=al.get(i);
		}
		return temp;
	}
	public static long getLongValue(byte[] b,int sta,int len){
		String hexString="";
		for(int i=len-1;i>=0;i--){
			String temp=Integer.toHexString(toNoSymbolInt(b[sta+i]));
			temp=temp.length()==1?0+temp:temp;
			hexString+=temp;
		}
		return Long.valueOf(hexString,16);
	}
	public static int getIntValue(byte[] b,int sta,int len){
		String hexString="";
		for(int i=len-1;i>=0;i--){
			String temp=Integer.toHexString(toNoSymbolInt(b[sta+i]));
			temp=temp.length()==1?0+temp:temp;
			hexString+=temp;
		}
		long value=Long.valueOf(hexString,16);		
		return (int)value;
	}
	public static void writeToFile(String text,String fileName) throws IOException{
		File file=new File(fileName);
		FileOutputStream fo=new FileOutputStream(file);
		fo.write(text.getBytes());
		//fo.flush();
		fo.close();
	}
	public static String convertTimeStr(int ms,String format){
		if(format==null || format.equals(""))
			format="hh:mm:ss";
		int hour=ms/(1000*60*60);
		int min=(ms-hour*1000*60*60)/(1000*60);
		int sec=(ms-(hour*60+min)*1000*60)/1000;
		if(hour==0){
			format=format.replace("hh:", "");
		}else
		if(min==0){
			format=format.replace("mm:", "");
		}
		if(hour==0){
			format=format.replace("hh:", "");
		}
		format=format.replace("hh", Integer.toString(hour));
		format=format.replace("mm", Integer.toString(min));
		format=format.replace("ss", Integer.toString(sec));
		return format;
	}
	public static String formatDateToString(Date date,String format){
		
		 DateFormat dateFormate = new java.text.SimpleDateFormat(format);
		
		return dateFormate.format(date);
	}
	public static void main(String[] args) throws IOException{
		System.out.println(GameUtil.convertTimeStr(680600,""));
	}
}
