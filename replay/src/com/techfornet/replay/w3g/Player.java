package com.techfornet.replay.w3g;

import java.io.Serializable;
import java.util.HashMap;

public class Player implements Serializable{
	public int runTime=0;
	public String race="";
	public int playerID=-1;
	public int recordID=-1;
	public String playerName="";
	public String color="";
	public int retraningTime=0;
	public String aiStr="";
	public int team=-1;
	public int playerFlag=-1;
	public int wonOrLost=-1;//0:lose 1:win
	
	public HashMap<String,Hero> heros=new HashMap();
	public HashMap<String,Item> items=new HashMap();
	public HashMap<String,Item> buildings=new HashMap();
	public HashMap<String,Item> upgrades=new HashMap();
	public HashMap<String,Item> units=new HashMap();
	public HashMap<Integer,HotKey> hotkeys=new HashMap();
	
	//action details
	public int actions=0;
	public int useAbility=0;
	public int buildingNum=0;
	public int unitsNum=0;
	public int itemNum=0;
	public int basicCommand=0;
	public int rightClick=0;
	public int itemGORD=0;
	public int select=0;
	public int assignHK=0;
	public int selectHk=0;
	public int subgroup=0;
	public int removeUnits=0;
	public int esc=0;
	public int heroMenu=0;
	public int buildMenu=0;
	
	//other
	public int unitMulti=1;
	public int unitsTime=0;
	public String lastItemid="";
	public int upgradesTime=0;
	public int runitsTime=0;
	public String runitsValue="";
	public int rupgradesTime=0;
	public String rupgradesValue="";
	
	public void assginHotkey(int group){
		if(!this.hotkeys.containsKey(group)){
			this.hotkeys.put(group, new HotKey(group));
		}else{
			this.hotkeys.get(group).assgined++;
		}
	}
	public void useHotKey(int group){
		if(this.hotkeys.containsKey(group))
			this.hotkeys.get(group).used++;
	}
	public void addHeros(String iden,String name,int time,int n){
		if(!this.heros.containsKey(iden)){
			//System.out.println(this.playerName+" "+name);
			heros.put(iden, new Hero(name,time));
		}else{
			//System.out.println(this.playerName+" "+name);
			heros.get(iden).addRTL(time,n);
		}
	}
	public void addUnits(String iden,String name,int time,int num){
		if(!this.units.containsKey(iden)){
			
			units.put(iden, new Item(name,time,num));
		}else{
			
			units.get(iden).addItem(name, time, num);
		}
	}
	public void addBuilding(String iden,String name,int time,int num){
		if(!this.buildings.containsKey(iden)){
			buildings.put(iden, new Item(name,time,num));
		}else{
			buildings.get(iden).addItem(name, time, num);
		}
	}
	public void addUpgrades(String iden,String name,int time,int num){
		if(!this.upgrades.containsKey(iden)){
			upgrades.put(iden, new Item(name,time,num));
		}else{
			upgrades.get(iden).addItem(name, time, num);
		}
	}
	public void addItems(String iden,String name,int time,int num){
		if(!this.items.containsKey(iden)){
			items.put(iden, new Item(name,time,num));
		}else{
			items.get(iden).addItem(name, time, num);
		}
	}
	
	public String toString(){
		StringBuffer temp=new StringBuffer();
		if(this.team!=12){
			temp.append("\n=================== "+this.playerName+" ======================");
			temp.append("\nPlayer: "+this.playerName);
			temp.append("\nRace: " + this.race);
			temp.append("\nColor: " + this.color);
			temp.append("\nRetraning Time: " + this.retraningTime);
			temp.append("\nTeam : "+this.team);
			temp.append("\nPlayer Flag: " + this.playerFlag);
			temp.append("\nWon Or Lost:" + (this.wonOrLost == 1 ? "Win" : "Lost"));
			temp.append("\nUse Ability : "+this.useAbility);
			temp.append("\nBuilding Num : "+this.buildingNum);
			temp.append("\nUnits Num: "+this.unitsNum);
			temp.append("\nItem Num: "+this.itemNum);
			temp.append("\nBasic Command: "+this.basicCommand);
			temp.append("\nRight Click: "+this.rightClick);
			temp.append("\nItem GORD: "+this.itemGORD);
			temp.append("\nSelect: "+this.select);
			temp.append("\nAssign HK: "+this.assignHK);
			temp.append("\nsSelect Hk: "+this.selectHk);
			temp.append("\nSub Group: "+this.subgroup);
			temp.append("\nRemove Units: "+this.removeUnits);
			temp.append("\nESC: "+this.esc);
			temp.append("\nHero Menu: "+this.heroMenu);
			temp.append("\nBuild Menu: "+this.buildMenu);
			temp.append("\n==============Heros===============");
			for(Hero hero : this.heros.values()){
				temp.append(hero+"\n");
			}
			temp.append("\n==============Buildings===============");
			for(Item item : this.buildings.values()){
				temp.append(item+"\n");
			}
			temp.append("\n==============Items===============");
			for(Item item : this.items.values()){
				temp.append(item+"\n");
			}
			temp.append("\n==============Units===============");
			for(Item item : this.units.values()){
				temp.append(item+"\n");
			}
			temp.append("\n==============Upgrades===============");
			for(Item item : this.upgrades.values()){
				temp.append(item+"\n");
			}
			temp.append("\n==============Hotkeys===============");
			for(HotKey hotKey : this.hotkeys.values()){
				temp.append(hotKey+"\n");
			}
		}
		return temp.toString();
	}
}
