package com.techfornet.replay.w3g;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Item implements Serializable{
	public String name="";
	public int num=0;
	public ArrayList<Integer[]> timeList=new ArrayList();
	public Item(String name,int time,int n){
		this.name=name;
		this.num+=n;
		this.addTimeList(time, n);
	}
	public void addItem(String name,int time,int n){
		this.name=name;
		this.num+=n;
		this.addTimeList(time, n);
	}
	public void addTimeList(int time,int n){
		if(n>=0){
			Integer[] temp=new Integer[]{time,n};
			timeList.add(temp);
		}
		if(n<0){
			int index=timeList.size()-1;
			timeList.remove(index);
		}
	}
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("\nName:" + this.name);
		buf.append(", Num:" + this.num);
		buf.append("\nTimeList:");
		for(Integer[] time : timeList){
			buf.append("Time:" + time[0] + " Num:" + time[1] + ", ");
		}
		return buf.toString();
	}
}
