package com.techfornet.replay.w3g;

import java.io.Serializable;

public class HotKey implements Serializable{
	public int assgined=0;
	public int used=0;
	public int group=-1;
	public HotKey(int g){
		this.group=g;
		this.used++;
		this.assgined++;
	}
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("\nGroup:" + this.assgined + ", Used:" + this.used + ", Assgined:" + this.assgined);
		return buf.toString();
	}
}
