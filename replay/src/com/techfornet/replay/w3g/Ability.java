package com.techfornet.replay.w3g;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Ability implements Serializable{
	public String name="";
	public int level;
	public ArrayList<Integer> resetLevel=new ArrayList();
	public ArrayList<Integer> levelTimes=new ArrayList();
	public ArrayList<HashMap<Integer,Integer>> useTimes=new ArrayList();
	public void setLevel(int num,int time){
		this.level+=num;
		this.levelTimes.add(time);
	}
	public Ability(String name,int time){
		this.setLevel(0, time);
		this.name=name;
		
	}
	public void reset(int time){
		this.resetLevel.add(this.level);
		this.level=0;
		this.levelTimes.add(0);		
	}
	public String toString(){
		String temp=" [name= "+
					this.name+"] [level="+this.level+"] ";
		
		return temp;
	}
}
