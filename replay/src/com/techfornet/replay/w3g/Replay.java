package com.techfornet.replay.w3g;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.techfornet.replay.w3g.GameConvert;
import com.techfornet.replay.w3g.Player;

public class Replay implements Serializable{
	private final int ACTIOND_ELAY=1000;
	private final int MAXDATA_BLOCK=1500;
	private final int RETRAINING_TIME=15000;
	
	
	public int time=0;
	public String timeStr="";
	private boolean pause=false;
	private int leaves=0;
	private int dataIndex=0;
	private byte[] data;
	private int index=0;
	private int subIndex=0;
	public HashMap<Integer,Player> players=new HashMap();
	private byte[] subData=null;
	
	//game info
	public int playerCount=0;
	public String gameSpeed="";
	public String visibility="";
	public String obServers="";
	public boolean teamTogether=false;
	public boolean lockTeams=false;
	public boolean controlShared=false;
	public boolean randomHero=false;
	public boolean randomRace=false;
	public String mapName="";
	public String creator="";
	private int slots=0;
	public String gameType="";
	public boolean privateFlag=false;
	public String selectMode="";
	private int startSpots=0;
	public int loserTeam=-1;//-1:draw 
	public int winnerTeam=-1;//-1:draw 
	private int saverID=-1;
	public String gameName="";
	private boolean continueGame=true;
	
	//chat[]
	public ArrayList<HashMap> chats=new ArrayList();	
	
	//header information
	int headerSize=0,war3V=0,dataBlocks=0,minorV=0,flags=0;
	String iden="";
	int majorV=0,buildV=0,gameFlag=0,w3gRunTime;
	int w3gFileSize,uSize,length;
	long checksum;
	
	//debug
	int debugindex=0;
	
	//convert
	private GameConvert gc;
	
	//parse param
	private int leaverUnkown=-1;
	private int leaverUnknown;
	
	public Replay(){}
	
	public Replay(String src) throws IOException{
		getData(src);
	}
	
	public boolean parseHeader(){
		String fileIntro=GameUtil.byteToString(data,0,26);
		index+=26;
		if(!fileIntro.equals("Warcraft III recorded game")){
			return false;
		}
		index+=2;
		headerSize=GameUtil.getIntValue(data, index, 4);
		index+=4;
		w3gFileSize=GameUtil.getIntValue(data, index, 4);
		index+=4;
		war3V=GameUtil.getIntValue(data, index, 4);
		index+=4;
		uSize=GameUtil.getIntValue(data, index, 4);
		index+=4;
		dataBlocks=GameUtil.getIntValue(data, index, 4);
		index=48;
		if(this.war3V<1){
			return false;
		}
		if(this.war3V==1){
			iden=GameUtil.byteToString(data, index, 4);
			index+=4;
			iden=new StringBuffer(iden).reverse().toString();
			majorV=GameUtil.getIntValue(data, index, 4);
			index+=4;
			buildV=GameUtil.getIntValue(data, index, 2);
			index+=2;
			gameFlag=GameUtil.getIntValue(data, index, 2);
			index+=2;
			w3gRunTime = (int) GameUtil.getLongValue(data, index, 4);
			index+=4;
			//checksum=;
			index+=4;
		}
		if(this.majorV<18){
			return false;
		}
		return true;
	}
	
	public void getDecompressData(){
		this.subData(this.headerSize);
		for(int i=0;i<this.dataBlocks;i++){
			
			//block header
			int cSize=0,uSize=0;
			cSize=GameUtil.getIntValue(data, index, 2);
			index+=2;
			uSize=GameUtil.getIntValue(data, index, 2);
			index+=2;
			index+=4;//checksum 
			byte[] compressDataAry=GameUtil.subAry(data, index, cSize);
			compressDataAry[2]|=1;
			byte[] subData=GameUtil.uncompressData(compressDataAry, 1024);
			this.subData(cSize+8);
			this.addBlockData(subData);
			
		}
	}
	public void parseData(){		
		this.getDecompressData();
		this.parsePlayerGame();
		this.parseBlocks();
	}
	public void parsePlayerGame(){
		Player player=new Player();
		subIndex=4;
		int recordID=GameUtil.getIntValue(subData, subIndex, 1);
		subIndex+=1;
		int playerID=GameUtil.getIntValue(subData, subIndex, 1);
		subIndex+=1;
		String playerName="";
		//for(;subData[subIndex]!=0x00;subIndex++){
		//	playerName+=(char)subData[subIndex];
		//}
		
		byte[] playerNameByte=null;
		for(;subData[subIndex]!=0x00;subIndex++){
			if(playerNameByte==null){
				playerNameByte=new byte[1];
				playerNameByte[0]=subData[subIndex];
			}else{
				byte[] tempByte=playerNameByte;
				int tempLen=tempByte.length;
				playerNameByte=null;
				playerNameByte=new byte[tempLen+1];
				System.arraycopy(tempByte, 0, playerNameByte, 0, tempLen);
				playerNameByte[tempLen]=subData[subIndex];
			}
			//content+=(char)subData[sindex];
		}
		try {
			playerName=new String(playerNameByte,"utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		subIndex+=1;
		if(subData[subIndex]==0x01){
			subIndex+=2;
		}
		if(subData[subIndex]==0x08){
			subIndex+=1;
			player.runTime=GameUtil.getIntValue(subData, subIndex, 4);
			subIndex+=4;
			player.race=gc.convertRace(GameUtil.getIntValue(subData, subIndex, 4));
			subIndex+=4;
		}
		player.playerID=playerID;
		player.recordID=recordID;
		player.playerName=playerName;
		players.put(playerID, player);
		playerCount++;
		//game
		String gameName="";
		//for(;subData[subIndex]!=0x00;subIndex++){
		//	gameName+=(char)subData[subIndex];
		//}
		
		byte[] gameNameByte=null;
		for(;subData[subIndex]!=0x00;subIndex++){
			if(gameNameByte==null){
				gameNameByte=new byte[1];
				gameNameByte[0]=subData[subIndex];
			}else{
				byte[] tempByte=gameNameByte;
				int tempLen=tempByte.length;
				gameNameByte=null;
				gameNameByte=new byte[tempLen+1];
				System.arraycopy(tempByte, 0, gameNameByte, 0, tempLen);
				gameNameByte[tempLen]=subData[subIndex];
			}
			//content+=(char)subData[sindex];
		}
		try {
			gameName=new String(gameNameByte,"utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println(gameName);
		this.gameName=gameName;
		subIndex+=2;
		String tempString="";
		int tempI=0;
		char mask = 0;
		for(;subData[subIndex]!=0;subIndex++){
			if(tempI%8==0){
				mask=(char) subData[subIndex];
			}else{
				if((mask&(char)(1<<(tempI%8)))==0){
					tempString+=(char)(subData[subIndex]-1);
				}else{
					tempString+=(char)subData[subIndex];
				}
			}
			tempI++;
		}
		subIndex++;
		char[] tempAry=tempString.toCharArray();
		gameSpeed=gc.convertSpeed((int)tempAry[0]);
		String visibility="";
		if(((int)tempAry[1]&1)!=0){
			visibility=gc.convertVisibility(0);
		}
		if(((int)tempAry[1]&2)!=0){
			visibility=gc.convertVisibility(1);
		}
		if(((int)tempAry[1]&4)!=0){
			visibility=gc.convertVisibility(2);
		}
		if(((int)tempAry[1]&8)!=0){
			visibility=gc.convertVisibility(3);
		}
		this.visibility=visibility;
		this.obServers=gc.convertObservers(convertBoolean((tempAry[1]&16)!=0)+2*(this.convertBoolean((tempAry[1]&32)!=0)));
		teamTogether=(tempAry[1]&64)!=0;
		lockTeams=tempAry[2]!=0;
		controlShared=(tempAry[3]&1)!=0;
		randomHero=(tempAry[3]&2)!=0;
		randomRace=(tempAry[3]&4)!=0;
		if((tempAry[3]&64)!=0){
			obServers=gc.convertObservers(4);
		}
		int tempAryIndex=13;
		String mapName="";
		for(;tempAry[tempAryIndex]!=0;tempAryIndex++){
			mapName+=(char)tempAry[tempAryIndex];
		}
		this.mapName=mapName;
		tempAryIndex++;
		for(;tempAry[tempAryIndex]!=0;tempAryIndex++){
			creator+=(char)tempAry[tempAryIndex];
		}
		
		//playercount or slots
		
		slots=GameUtil.getIntValue(subData, subIndex, 4);
		subIndex+=4;
		this.gameType=gc.convertGameType(subData[subIndex]);
		this.privateFlag=subData[subIndex+1]!=0;
		subIndex+=8;
		while(subData[subIndex]==0x16){//添加额外的player
			player=new Player();
			recordID=GameUtil.getIntValue(subData, subIndex, 1);
			subIndex+=1;
			playerID=GameUtil.getIntValue(subData, subIndex, 1);
			subIndex+=1;
			playerName="";
			//for(;subData[subIndex]!=0x00;subIndex++){
			//	playerName+=(char)subData[subIndex];
			//}
			
			playerNameByte=null;
			for(;subData[subIndex]!=0x00;subIndex++){
				if(playerNameByte==null){
					playerNameByte=new byte[1];
					playerNameByte[0]=subData[subIndex];
				}else{
					byte[] tempByte=playerNameByte;
					int tempLen=tempByte.length;
					playerNameByte=null;
					playerNameByte=new byte[tempLen+1];
					System.arraycopy(tempByte, 0, playerNameByte, 0, tempLen);
					playerNameByte[tempLen]=subData[subIndex];
				}
				//content+=(char)subData[sindex];
			}
			try {
				playerName=new String(playerNameByte,"utf8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			subIndex+=1;
			if(subData[subIndex]==0x01){
				subIndex+=2;
			}
			if(subData[subIndex]==0x08){
				subIndex+=1;
				player.runTime=GameUtil.getIntValue(subData, subIndex, 4);
				subIndex+=4;
				player.race=gc.convertRace(GameUtil.getIntValue(subData, subIndex, 4));
				subIndex+=4;
			}
			player.playerID=playerID;
			player.recordID=recordID;
			player.playerName=playerName;
			players.put(playerID, player);
			playerCount++;
			subIndex+=4;
		}
		
		int slotRecords,RecordLength;
		recordID=subData[subIndex];
		subIndex++;
		RecordLength=GameUtil.getIntValue(subData, subIndex, 2);
		subIndex+=2;
		slotRecords=subData[subIndex++];
		for(int i=0;i<slotRecords;i++){
			playerID=subData[subIndex++];
			//Player tempPlayer=this.players.get(playerID);
			subIndex++;
			int slotStauts=subData[subIndex++];
			//System.out.println(subData[subIndex+1]);
			int playerFlag=subData[subIndex++];
			int team=subData[subIndex++];
			int color=subData[subIndex++];
			int race=subData[subIndex++];
			int aiStr=subData[subIndex++];
			int handicap=subData[subIndex++];
			if(slotStauts==2){
				if(this.players.containsKey(playerID)){
					player=this.players.get(playerID);
				}else{
					player=new Player();
				}								
				player.playerFlag=playerFlag;
				player.team=team;
				player.color=gc.convertColor(color);
				player.aiStr=gc.convertAI(aiStr);
				//this.players.put(playerID, player);
			}
		}
		int randomSeed,selectMode,startSpots;
		randomSeed=GameUtil.getIntValue(subData, subIndex, 4);
		subIndex+=4;
		selectMode=subData[subIndex++];
		startSpots=subData[subIndex++];
		//subIndex+=4;
		this.selectMode=gc.convertSelectMode(selectMode);
		if(startSpots!=0xCC){
			this.startSpots=startSpots;
		}		
	}
	public void parseBlocks(){
		int dataLeftLen=subData.length-subIndex;
		//System.out.println(subData[subIndex]);
		while(dataLeftLen>0){
			//System.out.println(subData[subIndex]);
			int blockID=subData[subIndex];
			int reason,result,playerID,unknown;
			if(blockID==0x17){
				this.leaves++;
				int sindex=this.subIndex+1;
				reason=GameUtil.getIntValue(subData, sindex, 4);
				sindex+=4;
				playerID=GameUtil.getIntValue(subData, sindex++, 1);
				result=GameUtil.getIntValue(subData, sindex, 4);
				sindex+=4;
				Player player=this.players.get(playerID);
				player.runTime=this.time;
				unknown=GameUtil.getIntValue(subData, sindex, 4);
				//System.out.println(unknown);
				this.subIndex+=14;
				dataLeftLen-=14;
				if(this.leaverUnknown!=-1){
					this.leaverUnknown = unknown - this.leaverUnknown;
				}
				if(this.leaves==this.playerCount){
					this.saverID=playerID;
				}
				//System.out.println(reason+" "+result+" "+this.saverID+" "+leaverUnknown+" "+player.playerName);
				player.runTime=this.time;
				if(reason==0x01){
					switch(result){
						case 0x08:this.loserTeam=player.team;player.wonOrLost=0;break;							
						case 0x09:this.winnerTeam=player.team;player.wonOrLost=1;break;
						case 0x0A:break;//tie the default value of the loaseTeam and winnerTeam means the teams had been tie;
					}
				}//if0x01
				if(reason==0x0C && this.saverID!=-1){
					switch(result){
						case 0x07:
							if(this.leaverUnknown>0 && this.continueGame){
								this.winnerTeam=player.team;
								player.wonOrLost=1;
							}else{
								this.loserTeam=player.team;
								player.wonOrLost=0;
							}
							break;
						case 0x08:this.loserTeam=player.team;player.wonOrLost=0;break;
						case 0x09:this.winnerTeam=player.team;player.wonOrLost=1;break;
						case 0x0B:
							if(this.leaverUnknown>0){
								this.winnerTeam=player.team;
								player.wonOrLost=1;
							}
							break;
					}
				}//if 0x0C sva
				if(reason==0x0C && this.saverID==-1){
					switch(result){
						case 0x08:this.loserTeam=player.team;player.wonOrLost=0;break;
						case 0x09:this.winnerTeam=player.team;player.wonOrLost=1;break;
						case 0x0A:break;
					}
				}
				this.leaverUnknown=unknown;
			}else//if 0x17
			if(blockID==0x1A){
				this.subIndex+=5;
				dataLeftLen-=5;
			}else
			if(blockID==0x1B){
				this.subIndex+=5;
				dataLeftLen-=5;
			}else
			if(blockID==0x1C){//| 
				this.subIndex+=5;
				dataLeftLen-=5;
			}else//if0x1A
			if(blockID==0x1E){
				
			}else
			if(blockID==0x1F){// 
				int sindex=this.subIndex+1;
				int len=GameUtil.getIntValue(subData, sindex, 2);
				sindex+=2;
				int timeInc=GameUtil.getIntValue(subData, sindex, 2);
				if(!this.pause){
					this.time+=timeInc;
				}
				if(len>2){
					byte[] actionData=GameUtil.subAry(subData, this.subIndex+5, len-2);
					this.parseActions(actionData, len-2);
				}				
				this.subIndex+=len+3;
				dataLeftLen-=len+3;
				//System.out.println(this.time);
			}else//if 0x1E 0x1F
			if(blockID==0x20){
				int sindex=this.subIndex+1;
				playerID=this.subData[sindex++];
				int len=GameUtil.getIntValue(subData, sindex, 2);
				sindex+=2;
				int flags=this.subData[sindex++];
				int mode=GameUtil.getIntValue(subData, sindex, 4);
				sindex+=4;
				String content="";
				String modeStr=gc.convertChatMode(mode);
				String playerName=this.players.get(playerID).playerName;
				byte[] contentByte=null;
				for(;subData[sindex]!=0x00;sindex++){
					if(contentByte==null){
						contentByte=new byte[1];
						contentByte[0]=subData[sindex];
					}else{
						byte[] tempByte=contentByte;
						int tempLen=tempByte.length;
						contentByte=null;
						contentByte=new byte[tempLen+1];
						System.arraycopy(tempByte, 0, contentByte, 0, tempLen);
						contentByte[tempLen]=subData[sindex];
					}
				}
				this.subIndex+=len+4;
				dataLeftLen-=len+4;
				try {
					content=new String(contentByte,"utf8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				this.addChat(playerName, modeStr, content, this.time);
			}else//if 0x20
			if(blockID==0x22){
				int len=this.subData[this.subIndex+1];
				this.subIndex+=len+2;
				dataLeftLen-=len+2;
			}else
			if(blockID==0x23){
				this.subIndex+=11;
				dataLeftLen-=11;
			}else
			if(blockID==0x2F){
				this.subIndex+=9;
				dataLeftLen-=9;
			}else
			if(blockID==0x00){
				dataLeftLen=0;
			}else{
				break;
			}
			//break;
		}
	}
	
	public void parseActions(byte[] actionData,int dataLen){
		int blockLen=0;
		while(dataLen>0){
			if(blockLen>0){
				byte[] temp=actionData;
				actionData=null;
				actionData=GameUtil.subAry(temp, blockLen, temp.length-blockLen);
			}
			int n=0;
			int playerID=actionData[n++];
			blockLen=GameUtil.getIntValue(actionData, n, 2)+3;
			dataLen-=blockLen;
			boolean wasDeselect=false;
			boolean wasSubgroup=false;
			boolean wasSubupdate=false;
			n=3;
			int prev=0;
			int action=0;
			Player player=this.players.get(playerID);			
			byte[] bdata;
			int group=-1;
			int num=-1;
			while(n<blockLen){				
				prev=action;
				action=actionData[n];
				switch(action){
					case 0x10:
						player.actions++;
						if(this.majorV>=13){
							n++;
						}
						try{
                            bdata=GameUtil.subAry(actionData, n+2, 4);
                        }catch(Exception e){
                            break;
                        }
						if(bdata[2]==0x0D || bdata[3]==0x00){//技能的使用
							player.useAbility++;
							if(bdata[0]==0x33 && bdata[1] ==0x02 ){//如果是破坏者
								String name=gc.convertItems("ubsp").substring(2);
								player.addUnits("ubsp",name, this.time, 1);
								player.addUnits("uobs", name, time, -1);
							}
						}else{
							String itemid=new StringBuffer(new String(bdata)).reverse().toString();
							
							try{
								String[] names=gc.convertItems(itemid).split("_");
								String itemtype=names[0];
								String name=names[1];
								if(itemtype.equals("u")){
									//System.out.println(itemtype);
									this.parseUnits(player, itemid, name, this.time, player.unitMulti);
								}//if unints
								if(itemtype.equals("b")){
									this.parseBuildings(player, itemid, name, this.time, 1);
								}
								if(itemtype.equals("i")){
									this.parseItems(player, itemid, name, this.time, 1);
								}
								if(itemtype.equals("p")){
									this.parseUpgrades(player, itemid, name, this.time, 1);
								}
								if(itemtype.equals("h")){
									//System.out.println(this.time+" "+name+" "+player.playerName);
									this.parseHeros(player, itemid, name, this.time,1);
								}
								if(itemtype.equals("a")){
									this.parseHeroAbility(player, itemid, name, time);
								}
							}catch(Exception e){
								//e.printStackTrace();
								//System.out.println(gc.convertItems(itemid));
							}
							player.lastItemid=itemid;
						}// if else
						if(this.majorV>=7){
							n+=14;
						}else{
							n+=6;
						}
					    break;
					case 0x11:
						player.actions++;
						if(this.majorV>=13){
							n++;
						}
						try{
                            bdata=GameUtil.subAry(actionData, n+2, 4);
                        }catch(Exception e){
                            break;
                        }
						if(bdata[2]==0x0D && bdata[3]==0x00){
							this.parseActionDetails(bdata, player);
						}else{
							String itemid=new StringBuffer(new String(bdata)).reverse().toString();
							String name=gc.convertItems(itemid);
							if(!name.equals("")){
								this.parseBuildings(player, itemid, name, time, 1);
							}
							if(this.majorV>=7){
								n+=22;
							}else{
								n+=14;
							}
						}
						break;
					case 0x12:
						player.actions++;
						if(this.majorV>=13){
							n++;
						}
						try{
                            bdata=GameUtil.subAry(actionData, n+2, 4);
                        }catch(Exception e){
                            break;
                        }
						this.parseActionDetails(bdata, player);
						if(this.majorV>=7){
							n+=30;
						}else{
							n+=22;
						}
						break;
					case 0x13:
						player.actions++;
						if(this.majorV>=13){
							n++;
						}
						player.itemGORD++;
						if(this.majorV>=7){
							n+=38;
						}else{
							n+=30;
						}
						break;
					case 0x14:
						player.actions++;
						if(this.majorV>=13){
							n++;
						}
						try{
                            bdata=GameUtil.subAry(actionData, n+2, 4);
                        }catch(Exception e){
                            break;
                        }
						this.parseActionDetails(bdata, player);
						if(this.majorV>=7){
							n+=43;
						}else{
							n+=35;
						}
						break;
					case 0x16:
						try{
                            bdata=GameUtil.subAry(actionData, n+1, 3);
                        }catch(Exception e){
                            break;
                        }
						int mode=bdata[0];
						num=GameUtil.getIntValue(bdata, 1, 2);
						if(mode==0x02 || !wasDeselect){
							player.actions++;
							player.select++;
						}
						//
						wasDeselect = (mode == 0x02);
						player.unitMulti=num;
						n+=4+num*8;
						break;
					case 0x17:
						player.actions++;
						player.assignHK++;
						try{
                            bdata=GameUtil.subAry(actionData, n+1, 3);
                        }catch(Exception e){
                            break;
                        }
						group=bdata[0];
						num=GameUtil.getIntValue(bdata, 1, 2);
						player.assginHotkey(group);
						//System.out.println(num);
						n+=4+num*8;
						break;
					case 0x18:
						player.actions++;
						player.selectHk++;
						group=actionData[n+1];
						player.useHotKey(group);
						n+=3;
						break;
					case 0x19:
						if(this.buildV>=6040 || this.majorV >14){
							if(wasSubgroup){
								player.actions++;
								player.subgroup++;
								player.unitMulti=1;
							}
							n+=13;
						}else{
							wasSubgroup= actionData[n+1] == 0xff;
							n+=2;
						}	
						break;
					case 0x1A:
						if(this.buildV>=6040 || this.majorV>14){
							n+=1;
							wasSubgroup=(prev==0x19 || prev==0);
						}else{
							n+=10;
						}
						break;
					case 0x1B:
						n+=10;
						break;
					case 0x1C:
						player.actions++;
						n+=10;
						break;
					case 0x1D:
					case 0x1E:
						player.actions++;
						if((this.buildV>=6040 || this.majorV> 14)&& action!=0x1E){		
							n+=9;
						}else{
							
							player.removeUnits++;
							
							try{
	                            bdata=GameUtil.subAry(actionData, n+1, 3);
	                        }catch(Exception e){
	                            break;
	                        }
							this.parse0x1E(bdata,player);	
							n+=6;
						}
						break;
					case 0x21:
						n+=9;
						break;
					case 0x50:
						n+=6;
						break;
					case 0x51:
						n+=10;
						break;
					case 0x60:
						while(actionData[n]!=0x00){
							n++;
						}
						n+=2;
						break;
					case 0x61:
						player.actions++;
						player.esc++;
						n+=1;
						break;
					case 0x62:
						n+=13;
						break;
					case 0x65:
						player.actions++;
						player.heroMenu++;
						n+=1;
						break;
					case 0x66:
						player.actions++;
						player.heroMenu++;
						n+=1;
						break;
					case 0x67:
						player.actions++;
						player.buildMenu++;
						n+=1;
						break;
					case 0x68:
						n+=13;
						break;
					case 0x69:
					case 0x6A:
						this.continueGame=true;
						n+=17;
						break;
					case 0x01:
						this.pause=true;
						n+=1;
						break;
					case 0x02:
						this.pause=false;
						n+=1;
						break;
					case 0x03:
						n+=2;
						break;
					case 0x05:
						n+=1;
						break;
					case 0x06:
						try {
							while (actionData[n] != 0) {
								n++;
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						n+=1;
						break;
					case 0x07:
						n+=5;
						break;
					case 0x75:
						n+=2;
						break;
					default: n+=2;
				}
			}
			wasDeselect = (action==0x16);
			wasSubupdate = (action==0x19);
		}//while dataLen>0
	}	
	public void parse0x1E(byte bdata[],Player player){
		String itemid=new StringBuffer(new String(bdata)).reverse().toString();
		String value[]=gc.convertItems(itemid).split("_");
		String pre=value[0];
		String name=value[1];
		if(pre.equals("u")){
			if(this.time - player.runitsTime > this.ACTIOND_ELAY || !itemid.equals(player.runitsValue)){
				player.runitsTime = this.time;
				player.runitsValue= itemid;
				this.parseUnits(player, itemid, name, time, -1);
			}
		}
		if(pre.equals("b")){
			this.parseBuildings(player, itemid, name, time, -1);
		}
		if(pre.equals("h")){
			this.parseHeros(player, itemid, name, time, -1);
		}
		if(pre.equals("p")){
			if(this.time - player.rupgradesTime > this.ACTIOND_ELAY || !itemid.equals(player.rupgradesValue)){
				this.parseUpgrades(player, itemid, name, time, -1);
			}
		}
	}
	public void parseActionDetails( byte[] bdata,Player player){
		if(bdata[0]==0x03 && bdata[1]==0x00){
			
			player.rightClick++;
		}
		if(bdata[0]<=0x19 && bdata[1]==0x00){
			player.basicCommand++;
		}else{
			player.useAbility++;
		}
	}
	public void parseHeroAbility(Player player,String itemid,String name,int time){
		String names[] = name.split(":");
		String heroId=names[0];
		String aName=names[1];
		//System.out.println(aName);
		Hero hero=player.heros.get(heroId);
		if(!hero.abilities.containsKey(itemid)){
			hero.addAbility(itemid, aName, this.time);
		}
		Ability ability=hero.abilities.get(itemid);
		int retrainingTime=player.retraningTime;
		//System.out.println(this.time);
		if((this.time - hero.abilityTime > this.ACTIOND_ELAY
				|| hero.abilityTime==0 || this.time - retrainingTime < this.RETRAINING_TIME)
				&& ability.level < 3
		){
			if(this.time - retrainingTime > this.RETRAINING_TIME){
				
				hero.abilityTime = this.time;
				ability.setLevel(1, this.time);

				hero.addLevel(this.time);
				hero.isRetrn=false;
			}else{
				hero.retrainingTime=retrainingTime;
				hero.resetAbility();
				ability.setLevel(1, this.time);
			}
		}
	}
	public void parseHeros(Player player,String itemid,String name,int time,int n){
		player.addHeros(itemid, name, time, n);
	}
	public void parseBuildings(Player player,String itemid,String name,int time,int num){
		player.buildingNum+=num;
		player.addBuilding(itemid, name, this.time, num);
	}
	public void parseUnits(Player player,String itemid,String name,int time,int num){
		if(this.time - player.unitsTime >this.ACTIOND_ELAY || !itemid.equals(player.lastItemid)
				|| ((itemid.equals("hpea") || itemid.equals("ewsp") || itemid.equals("opeo") || itemid.equals("uaco")
						
				)&& this.time - player.unitsTime >0)
		){
			
			player.unitsNum+=num;
			player.addUnits(itemid, name, this.time, num);
		}
	}
	public void parseItems(Player player,String itemid,String name,int time,int num){
		player.itemNum+=num;
		player.addItems(itemid, name, this.time, num);
		if(itemid.equals("tret")){
			player.retraningTime=this.time;
		}
	}
	public void parseUpgrades(Player player,String itemid,String name,int time,int num){
		if(this.time - player.upgradesTime > this.ACTIOND_ELAY || !itemid.equals(player.lastItemid)){
			player.addUpgrades(itemid, name, this.time, num);
			player.upgradesTime=this.time;
		}
	}
	public int convertBoolean(boolean b){
		if(b){
			return 1;
		}
		return 0;
	}
	public void getData(String src) throws IOException{
		data=GameUtil.getW3gData(src, 1024);
	}
	public void setData(byte[] data){
		this.data=data;
	}
	public void subData(int size){
		byte[] temp=data;
		int len=temp.length-size;
		data=new byte[len];
		System.arraycopy(temp, size, data, 0, len);
		index=0;
	}
	public void addBlockData(byte[] bData){
		if(this.subData==null){
			this.subData=bData;
			return;
		}
		//System.out.println(this.subData.length);
		int len=0;
		byte[] temp;
		len=this.subData.length+bData.length;
		temp=this.subData;
		this.subData=null;
		this.subData=new byte[len];
		System.arraycopy(temp, 0, this.subData, 0, temp.length);
		System.arraycopy(bData, 0, this.subData, temp.length, bData.length);
	}
	public void addChat(String player,String toWho,String content,long time){
		HashMap chat=new HashMap();
		chat.put("player", player);
		chat.put("toWho", toWho);
		chat.put("content", content);
		chat.put("time", time);
		this.chats.add(chat);
	}
	public void cleanUp(){
		cleanUpWinner();
		cleanReplayInfo();
	}
	public void cleanUpWinner(){
		Set pidSet=this.players.keySet();
		Iterator it=pidSet.iterator();
		while(it.hasNext()){
			Player player=this.players.get(Integer.parseInt(it.next().toString()));
			if(player.team!=12){
				if(this.winnerTeam!=-1){
					if(player.team==this.winnerTeam){
						player.wonOrLost=1;
					}else{
						player.wonOrLost=0;
						this.loserTeam=player.team;
					}
				}
				if(this.loserTeam!=-1){
					if(player.team==this.loserTeam){
						player.wonOrLost=0;
					}else{
						player.wonOrLost=1;
						this.winnerTeam=player.team;
					}
				}
			}
		}
	}
	public void cleanReplayInfo(){
		this.timeStr=GameUtil.convertTimeStr(this.time, null);
	}
	public GameConvert getGc() {
		return gc;
	}

	public void setGc(GameConvert gc) {
		this.gc = gc;
	}

	public boolean parseReplay(){
		if(this.data.length==0)
			return false;
		if(!parseHeader())
			return false;
		parseData();
		cleanUp();
		//print();
		return true;
	}
	public boolean parseReplay(String w3gPath){
		try {
			getData(w3gPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}		
		return this.parseReplay();
	}
	
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("==================Game INFO====================");
		buf.append("\nGame Name:   " + this.gameName);
		buf.append("\nMap Name:    " + this.mapName);
		buf.append("\nCreator:     " + this.creator);
		buf.append("\nVersion:     " + this.war3V+ "." + this.majorV + "." + this.minorV + "." + this.buildV);
		buf.append("\nWinner Team: " + this.winnerTeam);
		buf.append("\nTime:        " + this.w3gRunTime);
		buf.append("\n=================Players======================");
		for(Player player : this.players.values()){
			buf.append(player);
		}
		return buf.toString();
	}

	public static void main(String[] args) throws IOException{
		Replay r=new Replay("D:\\0012.w3g");
		GameConvert gc = new GameConvert("com/techfornet/replay/w3g/gameconvert.properties");
		r.setGc(gc);
		r.parseReplay();	
		System.out.println(r);
	}
}