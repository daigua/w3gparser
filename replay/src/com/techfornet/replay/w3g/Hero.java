package com.techfornet.replay.w3g;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;

public class Hero implements Serializable{
	public String name="";
	public int level=0;
	public ArrayList<Integer> levelTime=new ArrayList();
	public int reviveTimes=0;
	public HashMap<String,Ability> abilities=new HashMap();
	public ArrayList<Integer> reviveTimeList=new ArrayList();
	public int abilityTime=0;
	public int retrainingTime=0;
	public boolean isRetrn=false;
	public Hero(String name,int time){
		this.name=name;
		this.addRTL(time,1);
		//System.out.println(name);
	}
	public void addLevel(int time){
		this.level++;
		this.levelTime.add(time);
	}
	public void addRTL(int time,int n){
		if(n==-1){
			int index=reviveTimeList.size()-1;
			this.reviveTimes--;
			this.reviveTimeList.remove(index);
		}else{
			this.reviveTimes++;
			this.reviveTimeList.add(time);
		}
	}
	public void addAbility(String itemid,String name,int time){
		this.abilities.put(itemid, new Ability(name,time));
	}
	public void resetAbility(){
		Set akey=this.abilities.keySet();
		Iterator it=akey.iterator();
		while(it.hasNext()){
			
		}
	}
	public String toString(){
		StringBuffer buf = new StringBuffer();
		buf.append("\n--------------"+this.name+"-----------------");
		buf.append("\nName:" + this.name);
		buf.append(", Level: " + this.level);
		buf.append(", Revive Times:" + this.reviveTimes );
		buf.append(", Ability Time:" + this.abilityTime);
		buf.append(", Retraining Times:" + this.retrainingTime);
		buf.append("\nLevel Times:");
		for(int time : this.levelTime){
			buf.append(time+", ");
		}
		return buf.toString();
	}
}
