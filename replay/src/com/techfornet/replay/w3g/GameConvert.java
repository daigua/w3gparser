package com.techfornet.replay.w3g;

import java.io.*;

import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.core.io.support.PropertiesLoaderUtils;


public class GameConvert implements Serializable{
	
	private Properties p;
	private FileInputStream in;
	public GameConvert(String pSrc){
		try {
			//ApplicationContext context=new ClassPathXmlApplicationContext("classpath:config/common.xml");
			//Resource resource=context.getResource("classpath:w3gconfig/gameconvert.properties");
		    //p.load(resource.getInputStream());
		    p=PropertiesLoaderUtils.loadAllProperties(pSrc);
		    //System.out.println(p.getProperty("race.human"));
		}catch (FileNotFoundException e) {
		    System.err.println(pSrc+"�����ļ�δ�ҵ�");
		    e.printStackTrace();
		}catch (Exception e) {
		    System.err.println("��ȡ�����ļ�"+pSrc+"����");
		    e.printStackTrace();
		}
	}
	 public  String getValue(String itemName,String defaultValue){
		 itemName=itemName.replaceAll(" ", "");
		 return p.getProperty(itemName,defaultValue);
	 }	
	 public String convertRace(int value){
		 String temp="race.";
		 switch(value){
		 	case 0x01: temp="human";break;
		 	case 0x02: temp="orc";break;
		 	case 0x04: temp="nightelf";break;
		 	case 0x08: temp="undead";break;
		 	case 0x20: temp="random";break;
		 }
		 return getValue(temp,"");
	 }
	 public String convertSpeed(int value){
			String itemName="speed.";
			switch(value){
				case 0: itemName+= "slow";break;
				case 1: itemName+= "normal";break;
				case 2: itemName+= "fast";break;
				case 3: itemName+= "unset";break;
			}
			return getValue(itemName,"Normal");
	}
	 public String convertVisibility(int v){
		 	String itemName="visibility.";
			switch(v){
				case 0: itemName+="hiddenterrain";break;
				case 1: itemName+="mapexplored";break;
				case 2: itemName+="alwaysvisible";break;
				case 3: itemName+="default";break;
			}
			return getValue(itemName,"default");
	}
	public String convertObservers(int v){
		String itemName="observers.";
		switch(v){
			case 0: itemName+="noobservers";break;
			case 2: itemName+="observersondefeat";break;
			case 3: itemName+="fullobservers";break;
			case 4: itemName+="referees";break;
		}
		return getValue(itemName,"");
	}
	public String convertGameType(int v){
	 	String itemName="type.";
		switch(v){
			case 1: itemName+="ladder1vs1/ffa";break;
			case 9: itemName+="customgame";break;
			case 13: itemName+="singleplayer/localgame";break;
			case 32: itemName+="ladderteamgame(at/rt)";break;
		}
		return getValue(itemName,"");
	}
	public String convertColor(int v){
	 	String itemName="color.";
		switch(v){
			case 0: itemName+="red";break;
			case 1: itemName+="blue";break;
			case 2: itemName+="teal";break;
			case 3: itemName+="purple";break;
			case 4: itemName+="yellow";break;
			case 5: itemName+="orange";break;
			case 6: itemName+="green";break;
			case 7: itemName+="pink";break;
			case 8: itemName+="gray";break;
			case 9: itemName+="lightblue";break;
			case 10: itemName+="darkgreen";break;
			case 11: itemName+="brown";break;
			case 12: itemName+="observer";break;
		}
		return getValue(itemName,"");
	}
	public String convertAI(int v){
	 	String itemName="ai.";
		switch(v){
			case 0: itemName+="easy";break;
			case 1: itemName+="normal";break;
			case 2: itemName+="insane";break;
		}
		return getValue(itemName,"");
	}
	public String convertSelectMode(int v){
	 	String itemName="selectmode.";
		switch(v){
			case 0: itemName+="team&raceselectable";break;
			case 1: itemName+="teamnotselectable";break;
			case 3: itemName+= "team&racenotselectable";break;
			case 4: itemName+= "racefixedtorandom";break;
			case 204: itemName+= "automatedmatchmaking(ladder)";break;
		}
		return getValue(itemName,"");
	}
	public String convertChatMode(int value){
		switch(value){
			case 0x00: return "All";
			case 0x01: return "Allies";
			case 0x02: return "Observers";
			case 0xFE: return "All";
			case 0xFF: return "All";
		}
		return Integer.toString(value-2);
	}
	public String convertItems(String itemid){
		return getValue(itemid,"");
	}
	 public static void main(String[] args){
		
	 }
}
